package com.ifountain.opsgenie.client.swagger.model;

/**
 * @author Orhun Dalabasmaz
 * @since 2018-11-16.
 */
public class Responder {
    private String id;
    private String username;
    private String type;

    public Responder() {
    }

    public String getId() {
        return id;
    }

    public Responder setId(String id) {
        this.id = id;
        return this;
    }

    public String getType() {
        return type;
    }

    public Responder setType(String type) {
        this.type = type;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public Responder setUsername(String username) {
        this.username = username;
        return this;
    }
}
