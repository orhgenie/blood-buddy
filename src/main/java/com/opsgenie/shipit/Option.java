package com.opsgenie.shipit;

/**
 * @author Orhun Dalabasmaz
 * @since 2018-11-16.
 */
public class Option {
    private String text;
    private String value;

    public Option(String text) {
        this.text = text;
        this.value = text;
    }

    public Option(String text, String value) {
        this.text = text;
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
