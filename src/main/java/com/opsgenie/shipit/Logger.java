package com.opsgenie.shipit;

/**
 * @author Orhun Dalabasmaz
 * @since 2018-11-15.
 */
public class Logger {

    public static void info(String message) {
        System.out.println("[INFO] " + message);
    }

    public static void error(String message) {
        System.err.println("[ERROR] " + message);
    }
}
