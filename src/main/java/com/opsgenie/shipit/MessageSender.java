package com.opsgenie.shipit;

import allbegray.slack.type.Attachment;
import allbegray.slack.webapi.method.chats.ChatPostMessageMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class MessageSender {

    @Autowired
    private AppConfigurer configurer;

    public void sendBloodTypeSelection(String channelId, String callbackId, String text) {
        ChatPostMessageMethod postMessage = new ChatPostMessageMethod(channelId, "Blood Buddy Helper");

        Attachment attachment = new Attachment();
        attachment.setCallback_id(callbackId);
        attachment.setText(text);

        ActionOptions action = new ActionOptions("bloodType", "select", "select");
        Arrays.stream(BloodType.values()).forEach(type -> action.getOptions().add(new Option(type.value)));
        attachment.getActions().add(action);

        postMessage.getAttachments().add(attachment);
        configurer.webApiClient.postMessage(postMessage);
    }

    public void sendMessage(String channelId, String text) {
        ChatPostMessageMethod postMessage = new ChatPostMessageMethod(channelId, "Blood Buddy Helper");
        postMessage.setChannel(channelId);
        postMessage.setText(text);
        configurer.webApiClient.postMessage(postMessage);
    }
}
