package com.opsgenie.shipit;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ButtonResponse {

    @JsonProperty(value = "callback_id")
    private String callbackId;
    private String name;
    private String value;

    public String getCallbackId() {
        return callbackId;
    }

    public void setCallbackId(String callbackId) {
        this.callbackId = callbackId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
