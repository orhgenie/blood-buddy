package com.opsgenie.shipit;

import allbegray.slack.SlackClientFactory;
import allbegray.slack.webapi.SlackWebApiClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class AppConfigurer {

    SlackWebApiClient webApiClient;

    @Value("${slackBotToken}")
    private String token;

    @PostConstruct
    void postConstruct() {
        webApiClient = SlackClientFactory.createWebApiClient(token);
    }
}
