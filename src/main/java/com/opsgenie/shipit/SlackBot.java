package com.opsgenie.shipit;

import me.ramswaroop.jbot.core.common.Controller;
import me.ramswaroop.jbot.core.common.EventType;
import me.ramswaroop.jbot.core.common.JBot;
import me.ramswaroop.jbot.core.slack.Bot;
import me.ramswaroop.jbot.core.slack.models.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.socket.WebSocketSession;

@JBot
public class SlackBot extends Bot {

    @Value(value = "${slackBotToken}")
    private String slackToken;

    @Autowired
    private MessageSender sender;

    @Override
    public String getSlackToken() {
        return slackToken;
    }

    @Override
    public Bot getSlackBot() {
        return this;
    }

    @Controller(events = {EventType.DIRECT_MENTION, EventType.DIRECT_MESSAGE})
    public void onReceiveDM(WebSocketSession session, Event event) {
        String text = event.getText().toLowerCase().trim();
        String userId = event.getUserId();
        if (text.startsWith("hi") || text.startsWith("hey")) {
            reply(session, event, "hey!");
        } else if ("help".equals(text)) {
            reply(session, event, "Available commands: register, show, search.");
        } else if ("register".equals(text)) {
            sender.sendBloodTypeSelection(event.getChannelId(), Constants.REGISTER_CALLBACK, "Register Your Blood Type");
        } else if ("show".equals(text)) {
            BloodType bloodType = DataStore.getBloodTypeOfUser(userId);
            String message = bloodType != null ? "You're blood type is \"" + bloodType.value + "\"" : "Not registered yet, type register.";
            reply(session, event, message);
        } else if ("search".equals(text)) {
            sender.sendBloodTypeSelection(event.getChannelId(), Constants.SEARCH_CALLBACK, "Select Blood Type You Are Looking For");
        } else {
            //reply(session, event, "command [" + text + "] not recognized, please try register, show, search.");
        }
    }
}
