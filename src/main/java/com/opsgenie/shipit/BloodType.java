package com.opsgenie.shipit;

import java.util.Arrays;

/**
 * @author Orhun Dalabasmaz
 * @since 2018-11-16.
 */
public enum BloodType {

    ArhP("A (+)"),
    ArhN("A (-)"),
    BrhP("B (+)"),
    BrhN("B (-)"),
    ABrhP("AB (+)"),
    ABrhN("AB (-)"),
    OrhP("O (+)"),
    OrhN("O (-)");

    String value;

    BloodType(String value) {
        this.value = value;
    }

    static BloodType valueFrom(String value) {
        return Arrays.stream(BloodType.values()).filter(bt -> bt.value.equals(value)).findFirst().orElse(null);
    }

    @Override
    public String toString() {
        return value;
    }
}
