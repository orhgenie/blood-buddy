package com.opsgenie.shipit;

import allbegray.slack.type.Action;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Orhun Dalabasmaz
 * @since 2018-11-15.
 */
public class ActionOptions extends Action {
    private List<Option> options = new ArrayList<>();

    public ActionOptions(String name, String text, String type) {
        super(name, text, type);
    }

    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }
}
