package com.opsgenie.shipit;

import java.util.*;

import static com.opsgenie.shipit.BloodType.*;

/**
 * @author Orhun Dalabasmaz
 * @since 2018-11-15.
 */
public class Constants {

    static final String REGISTER_CALLBACK = "bloodTypeSelectCallback";
    static final String SEARCH_CALLBACK = "bloodTypeSearchCallback";

    static final Map<BloodType, List<BloodType>> TRANSFUSE_MAP = new HashMap<>();

    static {
        TRANSFUSE_MAP.put(ABrhP, Arrays.asList(ArhP, ArhN, BrhP, BrhN, ABrhP, ABrhN, OrhP, OrhN));
        TRANSFUSE_MAP.put(ABrhN, Arrays.asList(ArhP, BrhP, ABrhP, OrhP));

        TRANSFUSE_MAP.put(ArhP, Arrays.asList(OrhP, OrhN, ArhP, ArhN));
        TRANSFUSE_MAP.put(ArhN, Arrays.asList(OrhN, ArhN));

        TRANSFUSE_MAP.put(BrhP, Arrays.asList(OrhP, OrhN, BrhP, BrhN));
        TRANSFUSE_MAP.put(BrhN, Arrays.asList(OrhN, BrhN));

        TRANSFUSE_MAP.put(OrhP, Arrays.asList(OrhP, OrhN));
        TRANSFUSE_MAP.put(OrhN, Collections.singletonList(OrhN));
    }
}
