package com.opsgenie.shipit;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.opsgenie.shipit.Constants.TRANSFUSE_MAP;

/**
 * @author Orhun Dalabasmaz
 * @since 2018-11-15.
 */
public class DataStore {

    private static final Map<String, BloodType> BLOOD_TYPES = new HashMap<>();
    private static final Map<String, String> USERS = new HashMap<>();

    public static void registerBloodType(String userId, String userName, BloodType bloodType) {
        BLOOD_TYPES.put(userId, bloodType);
        USERS.put(userId, userName);
    }

    public static List<String> getUsersWithBloodType(BloodType bloodType) {
        return BLOOD_TYPES.entrySet().stream()
                .filter(k -> bloodType.equals(k.getValue()))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    public static List<String> getUsersByNameWithBloodType(BloodType bloodType) {
        return BLOOD_TYPES.entrySet().stream()
                .filter(entry -> bloodType.equals(entry.getValue()))
                .map(entry -> USERS.get(entry.getKey()))
                .collect(Collectors.toList());
    }

    public static List<String> getUsersByNameWhoCanGiveBlood(BloodType bloodType) {
        List<BloodType> giverBloodTypes = TRANSFUSE_MAP.get(bloodType);
        return BLOOD_TYPES.entrySet().stream()
                .filter(entry -> giverBloodTypes.contains(entry.getValue()))
                .map(entry -> USERS.get(entry.getKey()))
                .collect(Collectors.toList());
    }

    public static BloodType getBloodTypeOfUser(String userId) {
        return BLOOD_TYPES.get(userId);
    }

    public static String getUsername(String userId) {
        return USERS.get(userId);
    }
}
