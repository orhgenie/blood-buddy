package com.opsgenie.shipit;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class MessageReceiver {

    @Autowired
    private NotificationManager notificationManager;

    @Autowired
    private MessageSender messageSender;

    @PostMapping(value = "rest/activity",
            produces = {"application/json", "application/xml"},
            consumes = {"application/x-www-form-urlencoded"})
    @SuppressWarnings("unchecked")
    public void activity(@RequestBody MultiValueMap paramMap) throws IOException {
        //Logger.info("request body: " + paramMap);
        String payload = (String) paramMap.getFirst("payload");
        Map<String, Object> result = new ObjectMapper().readValue(payload, HashMap.class);
        Map action = ((Map) ((List) result.get("actions")).get(0));
        String name = (String) action.get("name");
        String type = (String) action.get("type");
        String bloodTypeValue = null;
        if ("button".equals(type)) {
            bloodTypeValue = (String) action.get("value");
        } else if ("select".equals(type)) {
            bloodTypeValue = (String) ((Map) ((List) action.get("selected_options")).get(0)).get("value");
        }
        BloodType bloodType = BloodType.valueFrom(bloodTypeValue);

        String callbackId = (String) result.get("callback_id");
        String channelId = (String) ((Map) result.get("channel")).get("id");
        String userId = (String) ((Map) result.get("user")).get("id");
        String userName = ((String) ((Map) result.get("user")).get("name")).replaceAll("_", "+");

        if (Constants.REGISTER_CALLBACK.equals(callbackId)) {
            DataStore.registerBloodType(userId, userName, bloodType);
            messageSender.sendMessage(channelId, "Your blood type is registered: \"" + bloodType + "\"");
        } else if (Constants.SEARCH_CALLBACK.equals(callbackId)) {
            List<String> candidates = DataStore.getUsersByNameWhoCanGiveBlood(bloodType);
            Logger.info(DataStore.getUsername(userId) + " is searching for bloodType " + bloodType + " and found with users: " + candidates);

            if (candidates.isEmpty()) {
                Logger.info("No user found with blood type: " + bloodType);
                messageSender.sendMessage(channelId, "No user found with blood type: " + bloodType);
            } else {
                Logger.info("Sending alerts to users: " + candidates);
                messageSender.sendMessage(channelId, candidates.size() + " users will be notified for blood type: " + bloodType);
                candidates.forEach(candidate -> notificationManager.createAlert(userName, candidate, bloodType));
            }
        }
    }
}
