package com.opsgenie.shipit;

import com.ifountain.opsgenie.client.OpsGenieClient;
import com.ifountain.opsgenie.client.swagger.ApiException;
import com.ifountain.opsgenie.client.swagger.api.AlertApi;
import com.ifountain.opsgenie.client.swagger.model.*;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

/**
 * @author Orhun Dalabasmaz
 * @since 2018-11-15.
 */
@Service
public class NotificationManager {
    private AlertApi alertApi;

    //@Value("${og.api.key}")
    private String genieKey = "f88eac18-48ba-41b0-9d16-2d684a849439";
    private String serviceId = "548b78c5-6fa2-465b-b7b4-662667fc765a";

    public NotificationManager() {
        init();
    }

    private void init() {
        OpsGenieClient client = new OpsGenieClient();
        alertApi = client.alertV2();
        alertApi.getApiClient().setApiKey(genieKey);
        //client.setApiKey("ab5454992-fabb2-4ba2-ad44f-1af65ds8b5c079");
        //client.setApiKey(genieKey);
    }

    public void createIncident(String fromUser, List<String> toUsers, String bloodType) {

    }

    public void createAlert(String fromUser, String toUser, BloodType bloodType) {
        CreateAlertRequest request = new CreateAlertRequest();
        request.setMessage("Your friend [" + fromUser + "] needs you!" + " S/He is looking for blood type: " + bloodType);
        request.setAlias("bloodBuddy-" + fromUser + "-" + toUser);
        request.setDescription(fromUser + " is looking for blood type: " + bloodType);

        //request.setActions(Arrays.asList("ping", "restart"));
        request.setTags(Collections.singletonList("blood-buddy"));
        //request.setEntity("ApppServer1");
        request.setPriority(CreateAlertRequest.PriorityEnum.P3);
        //request.setUser(toUser + "@opsgenie.com");
        request.addResponder(new Responder().setType("user").setUsername(toUser + "@opsgenie.com"));
        //request.setNote("Alert created");


        try {
            SuccessResponse response = alertApi.createAlert(request);
            Float took = response.getTook();
            String requestId = response.getRequestId();
            String result = response.getResult();
        } catch (ApiException ex) {
            Logger.error("ApiEx: " + ex.getMessage());
        }
    }

    public void listAlerts() {
        try {
            ListAlertsRequest request = new ListAlertsRequest();
            request.setQuery("status: open");
            request.setLimit(20);
            request.setOrder(ListAlertsRequest.OrderEnum.DESC);
            request.setSort(ListAlertsRequest.SortEnum.CREATEDAT);

            ListAlertsResponse response = alertApi.listAlerts(request);
            String requestId = response.getRequestId();
            BaseAlert data = response.getData().get(0);
            String alertId = data.getId();
            String alertMessage = data.getMessage();
            System.out.println("~~ " + alertMessage);
        } catch (ApiException e) {
            e.printStackTrace();
        }
    }
}
